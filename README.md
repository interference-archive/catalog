# Interference Archive Catalog

Interference Archive's catalog runs on an digital archival system called Collective Access, which uses PHP and MySQL.

> TODO Insert description of Collective Access, what it does for hierarchical relationships, why we're using it and how we should continue to maintain it.

This repository includes:
- important files related to IA's custom configuration of Collective Access
- a guide for how to get an instance of the Collective Access and the catalog running.
- a ![wiki](/wiki) that provides additional documentation and resources from the IA Catalog Working Group, and the IA Digital Working Group.

*Eventually this repository will also include the frontend of the catalog as well, but we're working on maintaining the backend!*

**Have an issue? find a bug? or feature request?**
Let us know! Bugs, deficiencies, and feature requests for the catalog can be added and tracked in ![issues](issues). Someone working on this repository should be able to respond about your issue as soon as possible.

### Getting Started

There are two separate codebases that make up the CA platform: **Providence**, and **Pawtucket**. The key file that defines the database and credentials, where the CA instance lives on the server, where images are stored on the server, and more is the `setup.php` file.

**Our Server**
We're running the catalog on an Ubuntu 16.04 server with PHP 7, MySQL X.X, on a DigitalOcean Droplet with the IP address `142.93.10.155`.

The user `ia` has been created with `sudo` permissions to perform most server operations. It is only accessible via SSH authentication. If you'd like to have your key added you'll need to ![follow these instructions](generating-ssh-key-and-and-copying-it) and email your request and public key to ![tech@interferencearchive.org](mailto:tech@interferencearchive.org).

**Dependencies**
We followed this basic tutorial for getting an instance of Ubuntu 16.04 running. If you'd like to run CA and our catalog on your own server (or locally) here's the list of core features you'll need.
- git
- PHP (+CA requirements)
- MySQL
- Apache

### Installing Collective Access

#### Download
Navigate to the directory where CA will ultimately live and run:
```
cd /var/www/html
```
and then clone the git repository
```
git clone https://github.com/collectiveaccess/providence.git
```
On a normal Ubuntu install, perhaps a new server, CA could be installed in the root web folder `/var/www/html` but in our case we have Apache ![virtual hosts](link-to-apache-vhosts-setup) set up to map a few domains to different areas of our server which allows us to keep two separate instances of the catalog up, each with their own copies of the database at:
`/var/www/catalog.interferencearchive.org` which contains the main installation and stable environment for the archive.
`/var/www/dev-catalog.interferencearchive.org` contains a few duplicate instance for testing out new releases and features.

#### Installing Dependencies
For Collective Access we include the following php modules:
```
sudo apt-get install \
 php-common\
 php-mbstring\
 php-curl\
 php-json\
 php-mysql\
 php-iconv\
 php-imagick\
 php-zip\
 php-xml\
 php-gd\
 php-cli\
 php-mcrypt
```

#### Set up the Database

```
mysql -u root -p
```
The password for the root MySQL user is `archivefever!`

```
CREATE DATABASE ca1;
```

```
CREATE USER collectiveaccess@localhost IDENTIFIED BY 'password';
```

```
GRANT ALL PRIVILEGES ON ca0.* TO collectiveaccess@localhost;
```

```
FLUSH PRIVILEGES;
```

```
exit
```



Give the database a name and create a login for it with full read/write access. Note the login information - you'll need it later. You can use the MySQL command line or web-based tools like phpMyAdmin to create the database and login.


Clone the IA Catalog CA Profile Repository (with the CA xml file)

```
git clone https://github.com/InterferenceArchive/ia_collective_access_profile.git
```





Check out the earlier release to match the previous IA install
```
git checkout release-1.4
```

Note for the 1.4 version of CA you'll need PHP5.6 and all the mysql dependencies (very annoying)
```
sudo apt-get install -y php5.6 php5.6-mcrypt php5.6-mbstring php5.6-curl php5.6-cli php5.6-mysql php5.6-gd php5.6-intl php5.6-xsl php5.6-zip libapache2-mod-php5.6
```

```
mv setup.php-dist setup.php
```

```
sudo rsync -avP ~/providence/ /var/www/html/
```

Change the privileges so that the server can write to the necessary folders.
```
sudo chown -R ia:www-data *
```

Change the database credentials in the setup.php file
```
nano /var/www/html/setup.php
```


Copy the xml profile over to /var/..install/profiles/xml



Install CA using the predefined Interference XML file

(will take a long time and probably stall, view the instance in another web browser)


If migrating old instance of CA, import the database to the newly created one

```
mysql -u collectiveaccess -p ca1 < publicca-backup-2018-07-21.sql
```

Also import it to a backup database to test things:
```
mysql -u collectiveaccess -p ca2 < publicca-backup-2018-07-21.sql
```

[Add domain]




Write a frontend for the Providence Backend???


MIGRATING TO 1.5

Updating the database:
```
For migration 109: The CollectiveAccess authentication back-end had a major overhaul in this update. If you previously used the stock authentication configuration, there is no further action necessary. However, if you have made changes to your authentication.conf configuration file, chances are that you won't be able to log into your system. Please take a close look at the new stock file and change your local configuration accordingly.
```

and we received this error:
```
Error applying database migration 122: Can't DROP 'u_rule_code'; check that column/key exists; error was at query 2; query was DROP INDEX u_rule_code ON ca_metadata_dictionary_rules;
```





The local catalog server located in IA is at the IP address: 24.193.114.181

```
ssh root@24.193.114.181
```
You'll need access to a computer with an ssh key added to the root user of that server. Email tech@interferencearchive.org for more information or to get involved in maintaining the Archive's technology stack.

To make a complete backup of the local IA catalog here are the steps we will need to take:

1. Back up the database

```
mysqldump -u collectiveaccess -p ca0 -v | gzip > ~/db_backups/ca0-backup-$(date +%Y-%m-%d).sql.gz
mysqldump -u publiccauser_f -p publicca -v | gzip > ~/db_backups/publicca-backup-$(date +%Y-%m-%d).sql.gz
```
//TODO create a cron job that does this automatically

2. Back up collective access files to new server

Using rsync
```
rsync -rltv root@24.193.114.181:/root/db_backups/* ~/leguin-backups/database-backups/
```

```
rsync -rltv root@24.193.114.181:/srv/ca0/* ~/leguin-backups/ca0/
```

```
rsync -rltv root@24.193.114.181:/srv/publicca/* ~/leguin-backups/publicca/
```

### Helpful Commands

Find out what `php` modules are available to you by running:
```
apt-cache search php- | less
```

To get a human-readable list of what files and directories are taking up the most amount of space on the server, run:
```
du -sh * | sort -h
```

For a typical CollectiveAccess installation where media and configuration files are stored in the typical (and pre-configured) locations, and assuming that you are writing database snapshots into a "dumps" directory in a location outside of the web server root, you should be backing-up, at a minimum, the following directories:

```
/path/to/mysql/dumps
/path/to/collectiveaccess/app/conf
/path/to/collectiveaccess/media
/path/to/collectiveaccess/setup.php
````

### Appendix

https://www.backuphowto.info/how-backup-mysql-database-automatically-linux-users


**`ia` remote user SSH key fingerprint**




# README: Providence version 1.7.6
# README: Providence version 1.7.7
# README: Providence version 1.7.13

[![Build Status](https://secure.travis-ci.org/collectiveaccess/providence.png?branch=master)](http://travis-ci.org/collectiveaccess/providence)

### About CollectiveAccess

CollectiveAccess is a web-based suite of applications providing a framework for management, description, and discovery of complex digital and physical collections in museum, archival, and research contexts. It is comprised of two applications. Providence is the “back-end” cataloging component of CollectiveAccess. It is highly configurable and supports a variety of metadata standards, data types, and media formats. Pawtucket2 is CollectiveAccess' general purpose public-access publishing tool. It provides an easy way to create web sites around data managed with Providence. (You can learn more about Pawtucket2 at https://github.com/collectiveaccess/pawtucket2)

CollectiveAccess is freely available under the open source GNU Public License version 3.

### About CollectiveAccess 1.7.13

Version 1.7.13 is a maintenance release with these bug fixes and minor improvements:
* Resolve issue where when importing multiple media onto a single record via the data importer, the last imported media is tagged as primary rather than the first.
* Resolve issue where logging into accounts with numeric login names would fail 
* Correct various errors in database schema that would cause errors with recent versions of MariaDB
* Bump version on selected vendor libraries due to advisories
* Improve formatting of summary displays

Note that this version is not yet compatible with PHP version 8. Please use versions 7.3 or 7.4.


### Installation

First make sure your server meets all of the [requirements](https://manual.collectiveaccess.org/setup/systemReq.html). Then follow the [installation instructions](https://manual.collectiveaccess.org/setup/Installation.html). 


### Updating from a previous version

NOTE: The update process is relatively safe and rarely, if ever, causes data loss. That said BACKUP YOUR EXISTING DATABASE AND CONFIGURATION prior to updating. You almost certainly will not need the backup, but if you do you'll be glad it's there.

To update, decompress the CollectiveAccess Providence 1.7.13 tar.gz or zip file and replace the files in your existing installation with those in the update. Take care to preserve your media directory (`media/`), local configuration directory (`app/conf/local/`), any local print templates (`app/printTemplates/`) and your setup.php file.

If you are updating from a version prior to 1.7, you must recreate your existing setup.php as the format has changed. Rename the existing setup.php to `setup.php-old` and copy the version 1.7.13 setup.php template in `setup.php-dist` to `setup.php`. Edit this file with your database login information, system name and other basic settings. You can reuse the settings in your existing setup.php file as-is. Only the format of setup.php has changed. If you are updating from version 1.7.x you do not need to change your setup.php file.

Once the updated files are in place navigate in your web browser to the login screen. You will see this message:

```
Your database is out-of-date. Please install all schema migrations starting with migration #xxx. Click here to automatically apply the required updates.
```
 
The migration number may vary depending upon the version you're upgrading from. Click on the `here` link to begin the database update process. 

Version 1.7 introduced zoomable page media for multipage documents such as PDFs, Microsoft Word or Powerpoint. Systems migrated from pre-1.7 versions of CollectiveAccess will not have these zoomable media versions available causing the built-in document viewer to fail. If your system includes multipage documents you should regenerate the media using the command-line `caUtils` utility in `support/bin`. The command to run (assuming your current working directory is `support/`) is:

```
bin/caUtils reprocess-media
```

Be sure to run it as a user that has write permissions on all media. You do not need to reprocess media if you are updating from a 1.7.x system.


### Useful Links

* Web site: https://collectiveaccess.org
* Documentation: https://manual.collectiveaccess.org
* Demo: https://demo.collectiveaccess.org/
* System requirements: https://manual.collectiveaccess.org/setup/systemReq.html
* Forum: https://www.collectiveaccess.org/support
* Bug Tracker: https://clangers.collectiveaccess.org


### Other modules

Pawtucket2: https://github.com/collectiveaccess/pawtucket2 (The public access front-end application for Providence)
